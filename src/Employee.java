import java.io.Serializable;

class Employee implements Person, Serializable {
    private Integer cost;
    private Integer age;

    @Override
    public Integer getCost() {
        return this.cost;
    }

    @Override
    public Integer getAge() {
        return age;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public int compareTo(Person o) {
        return this.getAge().compareTo(o.getAge());
    }
}