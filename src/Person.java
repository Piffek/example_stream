interface Person extends Comparable<Person> {
    Integer getCost();
    Integer getAge();
}