import java.io.*;

public class Main {

    private static Person employeeFromStream;

    public static void main(String[] args) throws IOException {
        Person employee = new Employee();
        ((Employee) employee).setAge(25);
        ((Employee) employee).setCost(30);

        writeObiectToFile(employee);

        readStreamFromObiectAndShow();

    }

    private static void readStreamFromObiectAndShow() {
        try {
            FileInputStream file = fileWithSerializedObject("employee.txt");
            readFromStream(file);
            System.out.println(employeeFromStream.getCost());
        }catch (IOException e) {
            e.getMessage();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void writeObiectToFile(Person employee) {
        try {
            FileOutputStream file = fileToSave("employee.txt");
            writeToStream(file, employee);
        } catch (IOException e) {
            e.getMessage();
        }
    }

    private static void readFromStream(FileInputStream file) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(file);
        employeeFromStream = (Person) objectInputStream.readObject();
        objectInputStream.close();
    }

    private static void writeToStream(FileOutputStream file, Object employee) throws IOException {
        if(file != null) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(file);
            objectOutputStream.writeObject(employee);
            objectOutputStream.flush();
            objectOutputStream.close();
        }
    }

    private static FileOutputStream fileToSave(String file) throws FileNotFoundException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        return fileOutputStream;
    }

    private static FileInputStream fileWithSerializedObject(String file) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(file);
        return fileInputStream;
    }
}






